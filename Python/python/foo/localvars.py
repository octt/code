
# Print a list of global objects that don't start with '__'.
def _local_names(name, locals):
    print "\nselect %s local names:" % (name)
    for (k,v) in locals.items():
        if not k.startswith('__'):
            print "\t%s: %s is a (%s), callable (%s)" % (k, v, type(v), callable(v))
