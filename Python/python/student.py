#!/usr/bin/python

class student():
    
    count = 0
    
    def __init__(self, name="Troy"):
        self.name = name
        student.classcount()
        
    def print_name(this):
        print "Name is: %s" % (this.name)


    @classmethod
    def classcount(cls):      
        cls.count += 1

    @classmethod 
    def myid(cls):
        print "myid: %s" % cls
        
 
    @staticmethod
    def printcount():
        print "Test"


# Main
student1 = student()
student2 = student("Tom")
print "Student id: %s" % id(student1)
print "Student id: %s" % id(student2)

print "Count: %s" % student.count
student1.print_name()
student2.print_name()
print "Count: %s" % student.count

print student.myid()

print id(student)

print student.printcount()