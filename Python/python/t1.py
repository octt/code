#!/usr/bin/python

# Print a list of Global objects that do not start with a '__'.
def _names():
    print "\nselect globals:"
    for (k,v) in globals().items():
        if not k.startswith('__'):
            print "\t%s:  %s  is a (%s), callable (%s)" % (k, v, type(v), callable(v))

# Print all Global objects.
def _allnames():
  print "\nall globals:"
  for (k,v) in globals().items():
    print "\t%s:  %s  is a (%s), callable (%s)" % (k, v, type(v), callable(v))


# Demo function that just prints a name and creates a few variables.
def printname(name,age):
    person = True
    print "\n\nHi, my name is %s\n" % name
    _local_names("printname", locals())

    
#
#  Main
#
    
# dir
# 
#  Returns a list of the attributes and methods of any object: modules, functions, strings, lists, dictionaries. Objects.
#

print "\ndir (): %s" % dir()

from foo.localvars import _local_names

_allnames()

_names()
_local_names(__name__, locals())

a = 300

_names()
_local_names(__name__, locals())

printname("Tom",25)
printname("Julie",33)

import pip
print "%s" % dir(pip)

import pet
print "\n%s" % dir(pet)

pet.petname("spike","dog")

del pet

from pet import petname
petname("river", "bird")
