{
  "AWSTemplateFormatVersion": "2010-09-09",
  "Resources": {
    "vpc877d98e3": {
      "Type": "AWS::EC2::VPC",
      "Properties": {
        "CidrBlock": "10.0.0.0/16",
        "InstanceTenancy": "default",
        "EnableDnsSupport": "true",
        "EnableDnsHostnames": "false",
        "Tags": [
          {
            "Key": "Name",
            "Value": "CAKE-VPC"
          }
        ]
      }
    },
    "subnet919497e6": {
      "Type": "AWS::EC2::Subnet",
      "Properties": {
        "CidrBlock": "10.0.1.0/24",
        "AvailabilityZone": "us-west-2a",
        "VpcId": {
          "Ref": "vpc877d98e3"
        },
        "Tags": [
          {
            "Key": "Name",
            "Value": "SUB-CAKE-WEB"
          }
        ]
      }
    },
    "igwc9da94ac": {
      "Type": "AWS::EC2::InternetGateway",
      "Properties": {
        "Tags": [
          {
            "Key": "Name",
            "Value": "CAKE-GW"
          },
          {
            "Key": "project",
            "Value": "CAKE"
          }
        ]
      }
    },
    "dopt40fe7d25": {
      "Type": "AWS::EC2::DHCPOptions",
      "Properties": {
        "DomainName": "us-west-2.compute.internal",
        "DomainNameServers": [
          "AmazonProvidedDNS"
        ]
      }
    },
    "acleee8068a": {
      "Type": "AWS::EC2::NetworkAcl",
      "Properties": {
        "VpcId": {
          "Ref": "vpc877d98e3"
        }
      }
    },
    "rtbd3d12eb7": {
      "Type": "AWS::EC2::RouteTable",
      "Properties": {
        "VpcId": {
          "Ref": "vpc877d98e3"
        }
      }
    },
    "elbCAKEELB": {
      "Type": "AWS::ElasticLoadBalancing::LoadBalancer",
      "Properties": {
        "Subnets": [
          {
            "Ref": "subnet919497e6"
          }
        ],
        "HealthCheck": {
          "HealthyThreshold": "10",
          "Interval": "30",
          "Target": "TCP:80",
          "Timeout": "5",
          "UnhealthyThreshold": "2"
        },
        "ConnectionDrainingPolicy": {
          "Enabled": "true",
          "Timeout": "120"
        },
        "ConnectionSettings": {
          "IdleTimeout": "60"
        },
        "CrossZone": "true",
        "SecurityGroups": [
          {
            "Ref": "sgSGCAKEWEB"
          }
        ],
        "Listeners": [
          {
            "InstancePort": "80",
            "LoadBalancerPort": "80",
            "Protocol": "HTTP",
            "InstanceProtocol": "HTTP"
          }
        ],
        "Tags": [
          {
            "Key": "project",
            "Value": "CAKE"
          }
        ]
      }
    },
    "asgCAKEWEBASG": {
      "Type": "AWS::AutoScaling::AutoScalingGroup",
      "Properties": {
        "AvailabilityZones": [
          "us-west-2a"
        ],
        "Cooldown": "300",
        "DesiredCapacity": "1",
        "HealthCheckGracePeriod": "300",
        "HealthCheckType": "EC2",
        "MaxSize": "1",
        "MinSize": "1",
        "VPCZoneIdentifier": [
          {
            "Ref": "subnet919497e6"
          }
        ],
        "NotificationConfigurations": [
          {
            "TopicARN": "arn:aws:sns:us-west-2:039773487386:CAKE-ASG-STATUS-CHG",
            "NotificationTypes": "autoscaling:EC2_INSTANCE_LAUNCH"
          },
          {
            "TopicARN": "arn:aws:sns:us-west-2:039773487386:CAKE-ASG-STATUS-CHG",
            "NotificationTypes": "autoscaling:EC2_INSTANCE_LAUNCH_ERROR"
          },
          {
            "TopicARN": "arn:aws:sns:us-west-2:039773487386:CAKE-ASG-STATUS-CHG",
            "NotificationTypes": "autoscaling:EC2_INSTANCE_TERMINATE"
          },
          {
            "TopicARN": "arn:aws:sns:us-west-2:039773487386:CAKE-ASG-STATUS-CHG",
            "NotificationTypes": "autoscaling:EC2_INSTANCE_TERMINATE_ERROR"
          }
        ],
        "LaunchConfigurationName": {
          "Ref": "lcCAKELAUNCHINSTANCE"
        },
        "LoadBalancerNames": [
          {
            "Ref": "elbCAKEELB"
          }
        ],
        "Tags": [
          {
            "Key": "project",
            "Value": "CAKE",
            "PropagateAtLaunch": true
          }
        ],
        "MetricsCollection": [
          {
            "Metrics": "GroupMaxSize",
            "Granularity": null
          },
          {
            "Metrics": "GroupMinSize",
            "Granularity": null
          },
          {
            "Metrics": "GroupDesiredCapacity",
            "Granularity": null
          },
          {
            "Metrics": "GroupPendingInstances",
            "Granularity": null
          },
          {
            "Metrics": "GroupInServiceInstances",
            "Granularity": null
          },
          {
            "Metrics": "GroupTerminatingInstances",
            "Granularity": null
          },
          {
            "Metrics": "GroupTotalInstances",
            "Granularity": null
          }
        ],
        "TerminationPolicies": [
          "Default"
        ]
      }
    },
    "lcCAKELAUNCHINSTANCE": {
      "Type": "AWS::AutoScaling::LaunchConfiguration",
      "Properties": {
        "AssociatePublicIpAddress": true,
        "ImageId": "ami-f0091d91",
        "InstanceType": "t2.micro",
        "KeyName": "primary-kp",
        "IamInstanceProfile": "ROLE-CAKEWEB",
        "InstanceMonitoring": "true",
        "SecurityGroups": [
          {
            "Ref": "sgSGCAKEWEB"
          }
        ],
        "BlockDeviceMappings": [
          {
            "DeviceName": "/dev/xvda",
            "Ebs": {
              "VolumeSize": 8
            }
          }
        ]
      }
    },
    "s3cakeaccesslogs2": {
      "Type": "AWS::S3::Bucket",
      "Properties": {
        "AccessControl": "Private",
        "VersioningConfiguration": {
          "Status": "Suspended"
        }
      }
    },
    "s3cakeweb": {
      "Type": "AWS::S3::Bucket",
      "Properties": {
        "WebsiteConfiguration": {
          "IndexDocument": "index.htm",
          "ErrorDocument": "error.htm"
        },
        "AccessControl": "Private",
        "LoggingConfiguration": {
          "DestinationBucketName": "cake-accesslogs",
          "LogFilePrefix": "s3logs/"
        },
        "VersioningConfiguration": {
          "Status": "Suspended"
        },
        "Tags": [
          {
            "Key": "project",
            "Value": "CAKE"
          }
        ]
      }
    },
    "topicCAKEASG30less": {
      "Type": "AWS::SNS::Topic",
      "Properties": {
        "DisplayName": "CAKE-ASG-30less",
        "Subscription": [
          {
            "Endpoint": "korben@kirscht.com",
            "Protocol": "email"
          }
        ]
      }
    },
    "topicCAKEASG85plus": {
      "Type": "AWS::SNS::Topic",
      "Properties": {
        "DisplayName": "CAKE-ASG-85plus",
        "Subscription": [
          {
            "Endpoint": "korben@kirscht.com",
            "Protocol": "email"
          }
        ]
      }
    },
    "topicCAKEASGSTATUSCHG": {
      "Type": "AWS::SNS::Topic",
      "Properties": {
        "DisplayName": "CAKE-ASG-STATUS-CHG",
        "Subscription": [
          {
            "Endpoint": "korben@kirscht.com",
            "Protocol": "email"
          }
        ]
      }
    },
    "sgSGCAKEWEB": {
      "Type": "AWS::EC2::SecurityGroup",
      "Properties": {
        "GroupDescription": "CAKE Web Layer",
        "VpcId": {
          "Ref": "vpc877d98e3"
        },
        "Tags": [
          {
            "Key": "Name",
            "Value": "SG-CAKE-WEB"
          },
          {
            "Key": "project",
            "Value": "CAKE"
          }
        ]
      }
    },
    "snspolicyCAKEASG30less": {
      "Type": "AWS::SNS::TopicPolicy",
      "Properties": {
        "Topics": [
          {
            "Ref": "topicCAKEASG30less"
          }
        ],
        "PolicyDocument": {
          "Version": "2008-10-17",
          "Id": "__default_policy_ID",
          "Statement": [
            {
              "Sid": "__default_statement_ID",
              "Effect": "Allow",
              "Principal": {
                "AWS": "*"
              },
              "Action": [
                "SNS:GetTopicAttributes",
                "SNS:SetTopicAttributes",
                "SNS:AddPermission",
                "SNS:RemovePermission",
                "SNS:DeleteTopic",
                "SNS:Subscribe",
                "SNS:ListSubscriptionsByTopic",
                "SNS:Publish",
                "SNS:Receive"
              ],
              "Resource": {
                "Ref": "topicCAKEASG30less"
              },
              "Condition": {
                "StringEquals": {
                  "AWS:SourceOwner": "039773487386"
                }
              }
            }
          ]
        }
      }
    },
    "snspolicyCAKEASG85plus": {
      "Type": "AWS::SNS::TopicPolicy",
      "Properties": {
        "Topics": [
          {
            "Ref": "topicCAKEASG85plus"
          }
        ],
        "PolicyDocument": {
          "Version": "2008-10-17",
          "Id": "__default_policy_ID",
          "Statement": [
            {
              "Sid": "__default_statement_ID",
              "Effect": "Allow",
              "Principal": {
                "AWS": "*"
              },
              "Action": [
                "SNS:GetTopicAttributes",
                "SNS:SetTopicAttributes",
                "SNS:AddPermission",
                "SNS:RemovePermission",
                "SNS:DeleteTopic",
                "SNS:Subscribe",
                "SNS:ListSubscriptionsByTopic",
                "SNS:Publish",
                "SNS:Receive"
              ],
              "Resource": {
                "Ref": "topicCAKEASG85plus"
              },
              "Condition": {
                "StringEquals": {
                  "AWS:SourceOwner": "039773487386"
                }
              }
            }
          ]
        }
      }
    },
    "snspolicyCAKEASGSTATUSCHG": {
      "Type": "AWS::SNS::TopicPolicy",
      "Properties": {
        "Topics": [
          {
            "Ref": "topicCAKEASGSTATUSCHG"
          }
        ],
        "PolicyDocument": {
          "Version": "2008-10-17",
          "Id": "__default_policy_ID",
          "Statement": [
            {
              "Sid": "__default_statement_ID",
              "Effect": "Allow",
              "Principal": {
                "AWS": "*"
              },
              "Action": [
                "SNS:GetTopicAttributes",
                "SNS:SetTopicAttributes",
                "SNS:AddPermission",
                "SNS:RemovePermission",
                "SNS:DeleteTopic",
                "SNS:Subscribe",
                "SNS:ListSubscriptionsByTopic",
                "SNS:Publish",
                "SNS:Receive"
              ],
              "Resource": {
                "Ref": "topicCAKEASGSTATUSCHG"
              },
              "Condition": {
                "StringEquals": {
                  "AWS:SourceOwner": "039773487386"
                }
              }
            }
          ]
        }
      }
    },
    "s3policycakeaccesslogs2": {
      "Type": "AWS::S3::BucketPolicy",
      "Properties": {
        "Bucket": {
          "Ref": "s3cakeaccesslogs2"
        },
        "PolicyDocument": {
          "Version": "2012-10-17",
          "Id": "AWSConsole-AccessLogs-Policy-1455401189215",
          "Statement": [
            {
              "Sid": "AWSConsoleStmt-1455401189215",
              "Effect": "Allow",
              "Principal": {
                "AWS": "arn:aws:iam::797873946194:root"
              },
              "Action": "s3:PutObject",
              "Resource": {
                "Fn::Join": [
                  "",
                  [
                    "arn:aws:s3:::",
                    "cake-accesslogs",
                    "2/elb/AWSLogs/039773487386/*"
                  ]
                ]
              }
            }
          ]
        }
      }
    },
    "scalingDecreaseGroupSize": {
      "Type": "AWS::AutoScaling::ScalingPolicy",
      "Properties": {
        "AdjustmentType": "ChangeInCapacity",
        "AutoScalingGroupName": {
          "Ref": "asgCAKEWEBASG"
        }
      }
    },
    "scalingIncreaseGroupSize": {
      "Type": "AWS::AutoScaling::ScalingPolicy",
      "Properties": {
        "AdjustmentType": "ChangeInCapacity",
        "AutoScalingGroupName": {
          "Ref": "asgCAKEWEBASG"
        }
      }
    },
    "alarmawsec2ASGCAKECPUUtilization": {
      "Type": "AWS::CloudWatch::Alarm",
      "Properties": {
        "ActionsEnabled": "true",
        "ComparisonOperator": "GreaterThanOrEqualToThreshold",
        "EvaluationPeriods": "2",
        "MetricName": "CPUUtilization",
        "Namespace": "AWS/EC2",
        "Period": "300",
        "Statistic": "Average",
        "Threshold": "80.0",
        "AlarmActions": [
          "arn:aws:sns:us-west-2:039773487386:CAKE-ASG-85plus",
          {
            "Ref": "scalingIncreaseGroupSize"
          }
        ],
        "Dimensions": [
          {
            "Name": "AutoScalingGroupName",
            "Value": "ASG-CAKE"
          }
        ]
      }
    },
    "alarmawsec2ASGCAKELowCPUUtilization": {
      "Type": "AWS::CloudWatch::Alarm",
      "Properties": {
        "ActionsEnabled": "true",
        "ComparisonOperator": "LessThanThreshold",
        "EvaluationPeriods": "2",
        "MetricName": "CPUUtilization",
        "Namespace": "AWS/EC2",
        "Period": "300",
        "Statistic": "Average",
        "Threshold": "30.0",
        "AlarmActions": [
          "arn:aws:sns:us-west-2:039773487386:CAKE-ASG-30less",
          {
            "Ref": "scalingDecreaseGroupSize"
          }
        ],
        "Dimensions": [
          {
            "Name": "AutoScalingGroupName",
            "Value": "ASG-CAKE"
          }
        ]
      }
    },
    "acl1": {
      "Type": "AWS::EC2::NetworkAclEntry",
      "Properties": {
        "CidrBlock": "0.0.0.0/0",
        "Egress": "true",
        "Protocol": "-1",
        "RuleAction": "allow",
        "RuleNumber": "100",
        "NetworkAclId": {
          "Ref": "acleee8068a"
        }
      }
    },
    "acl2": {
      "Type": "AWS::EC2::NetworkAclEntry",
      "Properties": {
        "CidrBlock": "0.0.0.0/0",
        "Protocol": "-1",
        "RuleAction": "allow",
        "RuleNumber": "100",
        "NetworkAclId": {
          "Ref": "acleee8068a"
        }
      }
    },
    "subnetacl1": {
      "Type": "AWS::EC2::SubnetNetworkAclAssociation",
      "Properties": {
        "NetworkAclId": {
          "Ref": "acleee8068a"
        },
        "SubnetId": {
          "Ref": "subnet919497e6"
        }
      }
    },
    "gw1": {
      "Type": "AWS::EC2::VPCGatewayAttachment",
      "Properties": {
        "VpcId": {
          "Ref": "vpc877d98e3"
        },
        "InternetGatewayId": {
          "Ref": "igwc9da94ac"
        }
      }
    },
    "route1": {
      "Type": "AWS::EC2::Route",
      "Properties": {
        "DestinationCidrBlock": "0.0.0.0/0",
        "RouteTableId": {
          "Ref": "rtbd3d12eb7"
        },
        "GatewayId": {
          "Ref": "igwc9da94ac"
        }
      },
      "DependsOn": "gw1"
    },
    "dchpassoc1": {
      "Type": "AWS::EC2::VPCDHCPOptionsAssociation",
      "Properties": {
        "VpcId": {
          "Ref": "vpc877d98e3"
        },
        "DhcpOptionsId": {
          "Ref": "dopt40fe7d25"
        }
      }
    },
    "ingress1": {
      "Type": "AWS::EC2::SecurityGroupIngress",
      "Properties": {
        "GroupId": {
          "Ref": "sgSGCAKEWEB"
        },
        "IpProtocol": "tcp",
        "FromPort": "80",
        "ToPort": "80",
        "CidrIp": "10.0.0.0/16"
      }
    },
    "ingress2": {
      "Type": "AWS::EC2::SecurityGroupIngress",
      "Properties": {
        "GroupId": {
          "Ref": "sgSGCAKEWEB"
        },
        "IpProtocol": "tcp",
        "FromPort": "80",
        "ToPort": "80",
        "CidrIp": "66.215.226.168/32"
      }
    },
    "ingress3": {
      "Type": "AWS::EC2::SecurityGroupIngress",
      "Properties": {
        "GroupId": {
          "Ref": "sgSGCAKEWEB"
        },
        "IpProtocol": "tcp",
        "FromPort": "22",
        "ToPort": "22",
        "CidrIp": "66.215.226.168/32"
      }
    },
    "egress1": {
      "Type": "AWS::EC2::SecurityGroupEgress",
      "Properties": {
        "GroupId": {
          "Ref": "sgSGCAKEWEB"
        },
        "IpProtocol": "-1",
        "CidrIp": "0.0.0.0/0"
      }
    }
  },
  "Description": "Korben's Cloudformation template for CAKE."
}