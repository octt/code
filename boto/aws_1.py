#!/usr/bin/python

import boto

from os import environ

print environ['HOME']

# boto.set_stream_logger('boto')

conn = boto.connect_s3(environ['AWSAccessKeyId'], environ['AWSSecretKey'])

from boto.s3.connection import Location

print "Amazon S3 Locations"
print [x for x in dir(Location) if x.isalnum()]

# Traceback (most recent call last):
#   File "./aws_1.py", line 16, in <module>
#     conn.create_bucket('octtbucket', location=Location.USWest)
#   File "/usr/local/lib/python2.7/dist-packages/boto/s3/connection.py", line 617, in create_bucket
#     response.status, response.reason, body)
# boto.exception.S3CreateError: S3CreateError: 409 Conflict
try:
    print "Create bucket if it doesn't exist"
    conn.create_bucket('octtbucket', location=Location.USWest)
except boto.exception.S3CreateError:
    print "Error: boto.exception.S3CreateError"
    print "Bucket already exists."
else:
    print "Bucket exists"

#    Traceback (most recent call last):
#  File "./aws_1.py", line 33, in <module>
#    bucket = conn.get_bucket('octt1bucket')
#  File "/usr/local/lib/python2.7/dist-packages/boto/s3/connection.py", line 503, in #get_bucket
#    return self.head_bucket(bucket_name, headers=headers)
#  File "/usr/local/lib/python2.7/dist-packages/boto/s3/connection.py", line 547, in #head_bucket
#    raise err
#boto.exception.S3ResponseError: S3ResponseError: 404 Not Found

try:
    bucket = conn.get_bucket('octtbucket')
except boto.exception.S3ResponseError:
    print "Unexpected error - bucket does not exist"
    exit(1)

from boto.s3.key import Key
key = Key(bucket)

key.key = 'lumberjack_song.txt'

#Traceback (most recent call last):
#  File "./aws_1.py", line 50, in <module>
#    key.set_contents_from_filename('~/lumberjack_song.txt')
#  File "/usr/local/lib/python2.7/dist-packages/boto/s3/key.py", line 1358, in #set_contents_from_filename
#    with open(filename, 'rb') as fp:
#IOError: [Errno 2] No such file or directory: '~/lumberjack_song.txt'


filename = '~/lumberjack_song.txt'
try:
    key.set_contents_from_filename(filename)
except IOError:
    print "File %s - No such file or directory" % (filename)
    
filename = '/home/octt/lumberjack_song.txt'
try:
    key.set_contents_from_filename(filename)
except IOError:
    print "File %s - No such file or directory" % (filename)

key = bucket.get_key(key.key)
filename = '/home/octt/parrot.txt'
try:
    key.get_contents_to_filename(filename)
except IOError:
    print "File %s - No such file or directory" % (filename)

print "Bucket List"
buckets = conn.get_all_buckets()
print [b.name for b in buckets]

print [k.name for k in bucket.list()]