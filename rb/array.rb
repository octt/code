#!/usr/bin/ruby -w

array1 = [1,2,3,4,5,6,7,8,9,10]

array1.each do |i|
    puts i
end

for key in array1
    puts key
end

hash = colors = { "red" => 0xf00, "green" => 0x0f0, "blue" => 0x00f }
hash.each do |key, value|
   print key, " is ", value, "\n"
end

for key,value in hash
    puts "key is #{key}, value is #{value}"
end

(10..15).each do |n| 
   print n, ' ' 
end

for key in (10..15)
    puts key
end

for key in (10...15)
    puts key
end