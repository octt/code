#!/usr/bin/ruby -w

print <<EOF
    This is the first way of creating
    here document ie. multiple line string.
EOF

print <<"EOF";                # same as above
    This is the second way of creating
    here document ie. multiple line string.
EOF

print <<`EOC`                 # execute commands
	echo hi there
	echo lo there
EOC

print <<"foo", <<"bar"  # you can stack them
	I said foo.
foo
	I said bar.
bar

y = 5

x = <<EOF

    Assign a here document to a variable.

    y is #{y}

EOF

print x

END {
   puts "Terminating Ruby Program"
}

BEGIN {
   puts "Initializing Ruby Program"
}

=begin

    Documentation is a necessity.

    And must be included.

=end

require './vehicle.rb'

vehicle1 = Vehicle.new(4,379,"20gal",20)
vehicle1.info

vehicle2 = Vehicle.new(3,210,"10gal",10)
vehicle2.info

Vehicle.count