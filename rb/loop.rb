#!/usr/bin/ruby

$i = 0
$num = 5

while $i < $num  do
   puts("Inside the loop i = #$i" )
   $i +=1
end

$i = 0
$num = 5
begin
   puts("Inside the loop i = #$i" )
   $i +=1
end while $i < $num
    
$i = 0
$num = 5

until $i > $num - 1  do
   puts("Inside the loop i = #$i" )
   $i +=1;
end

$i = 0
$num = 5

for n in $i..$num
   puts "Value of local variable is #{n}"
end