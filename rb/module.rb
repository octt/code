#!/usr/bin/ruby -w

$LOAD_PATH << '.'

require 'trig.rb'
require 'moral'

y = Trig.sin(Trig::PI/4)
wrongdoing = Moral.sin(Moral::VERY_BAD)

puts y
puts wrongdoing
puts Moral.sin(Moral::BAD)