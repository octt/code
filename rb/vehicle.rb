    class Vehicle
        
        @@no_of_vehicles = 0
        
        def initialize(no_of_wheels,horsepower,type_of_tank,capacity)
            @no_of_wheels = no_of_wheels
            @horsepower = horsepower
            @type_of_tank = type_of_tank
            @capacity = capacity
            @@no_of_vehicles += 1
        end
        
        def info
            print "Number of wheels #{@no_of_wheels}\n"
        end
        
        def self.count
            print "Number of vehicles #{@@no_of_vehicles}\n"
        end
        
        def speeding
        end
        
        def driving
        end
        
        def halting
        end
        
    end