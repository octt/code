# Configure the VMware vSphere Provider
provider "vsphere" {
   user           = "root"
   password       = "1KR0nline"
   vsphere_server = "192.168.1.48"
   allow_unverified_ssl = true
#  user           = "${var.vsphere_user}"
#  password       = "${var.vsphere_password}"
#  vsphere_server = "${var.vsphere_server}"
}

# Create a folder
#resource "vsphere_folder" "frontend" {
#  path = "frontend"
#}

# Create a virtual machine within the folder
resource "vsphere_virtual_machine" "web" {
  name   = "terraform_web"
  image = "PcktTrcr"
#  folder = "${vsphere_folder.frontend.path}"
  vcpu   = 2
  memory = 4096

  network_interface {
    label = "VM Network"
  }

  disk {
#    template = "PcktTrcr"
    datastore = "datastore4"
    size = 8
  }
}
